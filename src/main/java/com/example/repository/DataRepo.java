package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.model.Data;

@Repository("dataRepo")
public interface DataRepo extends JpaRepository<Data, Integer>{

	@Query(value = "SELECT d FROM Data d WHERE d.month = :month ORDER BY d.percentage")
	List<Data> findAllByMonth(@Param("month") String month);

	@Query(value = "SELECT DISTINCT d.month FROM Data d")
	List<String> distinctByMonth();
}
