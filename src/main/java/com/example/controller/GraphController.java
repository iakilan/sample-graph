package com.example.controller;

import java.util.List;

import com.example.model.DataResponse;
import com.example.service.IDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class GraphController {

	@Autowired
    private IDataService dataService;

    @GetMapping("/welcome")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name");
        return "welcome";
    }
    
    @GetMapping("/loaddata")
    public  @ResponseBody List<DataResponse> greetings(@RequestParam(name="name", required=false, defaultValue="World") String name) {

    	return dataService.getListByMonth(name);
    }

    @GetMapping("/getmonth")
    public  @ResponseBody List<String> getMonth(@RequestParam(name="name", required=false, defaultValue="JUNE") String name) {

        return dataService.getMonth();
    }

}
