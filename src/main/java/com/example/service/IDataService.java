package com.example.service;

import java.util.List;

import com.example.model.DataResponse;

public interface IDataService {
	
	public List<DataResponse> getList(String month);
	public List<DataResponse> getListByMonth(String month);
	public List<String> getMonth();
}
