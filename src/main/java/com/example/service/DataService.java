package com.example.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.model.DataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.model.Data;
import com.example.repository.DataRepo;

@Service("dataService")
public class DataService implements IDataService {

	@Autowired
	private DataRepo dataRepo;

	@Transactional
	@Override
	public List<DataResponse> getList(String month) {
		List<Data> list  = dataRepo.findAll(new Sort(Sort.Direction.ASC,"percentage"));
		List<Data> res = list.stream().filter(l -> l.getMonth().equals(month) ).collect(Collectors.toList());
		List<DataResponse> listData = new ArrayList<>();
		res.forEach(l -> listData.add(new DataResponse(l.getDistrict(),l.getPercentage())));
		return listData;
	}

	@Transactional
	@Override
	public List<DataResponse> getListByMonth(String month) {
		List<Data> list = dataRepo.findAllByMonth(month);
		List<Data> res = list.stream().filter(l -> l.getMonth().equals(month) ).collect(Collectors.toList());
		List<DataResponse> listData = new ArrayList<>();
		res.forEach(l -> listData.add(new DataResponse(l.getDistrict(),l.getPercentage())));
		return listData;
	}

	@Override
	@Transactional
	public List<String> getMonth() {
		return dataRepo.distinctByMonth();
	}
}
